#include "common.hpp"
#include "GameObjects.hpp"

std::list<Asteroid *> objects;
std::list<Bullet *> bullets;
Button *buttonRight;
Button *buttonLeft;
Button *buttonThrottle;
Button *buttonShoot;
Player *pl;

GLfloat color[] = {
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,		
	};

/**
 * Our saved state data.
 */
struct saved_state {
    float angle;
    int32_t x;
    int32_t y;
};
	
/**
 * Shared state for our app.
 */
struct engine {
    struct android_app* app;

    ASensorManager* sensorManager;
    const ASensor* accelerometerSensor;
    ASensorEventQueue* sensorEventQueue;

    int animating;
    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;
    int32_t width;
    int32_t height;
    struct saved_state state;
};

static int engine_init_display(struct engine* engine) {
    // initialize OpenGL ES and EGL
    const EGLint attribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_NONE
    };
    EGLint w, h, dummy, format;
    EGLint numConfigs;
    EGLConfig config;
    EGLSurface surface;
    EGLContext context;

    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    eglInitialize(display, 0, 0);
    eglChooseConfig(display, attribs, &config, 1, &numConfigs);
    eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
    ANativeWindow_setBuffersGeometry(engine->app->window, 0, 0, format);
    surface = eglCreateWindowSurface(display, config, engine->app->window, NULL);
    context = eglCreateContext(display, config, NULL, NULL);

    if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
        LOGW("Unable to eglMakeCurrent");
        return -1;
    }

    eglQuerySurface(display, surface, EGL_WIDTH, &w);
    eglQuerySurface(display, surface, EGL_HEIGHT, &h);

    engine->display = display;
    engine->context = context;
    engine->surface = surface;
    engine->width = w;
    engine->height = h;
    engine->state.angle = 0;

	LOGI("ViewPort size: %dx%d", w, h);

    // Initialize GL state.
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);

	glOrthof(0, w, h, 0, 1, 1000);
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(0, 0, -500);
	
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	
	srand(time(NULL));
	
    return 0;
}

void init_game_objects(struct engine* engine)
{
	pl = new Player(engine->width / 2, engine->height / 2);

	for (int i = 0; i < 8; i++)
	{
		objects.push_back(new Asteroid( -rand() % 300, -rand() % 300, 10, BIG_ROCK));
	}
	
	buttonLeft		=	new Button( 200,						engine->height - 100,	100);
	buttonRight		=	new Button( 400,						engine->height,			100);	
	buttonThrottle	=	new Button( engine->width,				engine->height - 100,	100);
	buttonShoot		=	new Button( engine->width - 200,		engine->height,			100);	

	pl->dead = false;
}

void spawn(struct engine* engine)
{
	for (int i = 0; i < 8; i++)
	{
		objects.push_back(new Asteroid( -rand() % 300,-rand() % 300, 10, BIG_ROCK));
	}
}

void game_logic_update(struct engine* engine)
{
	if (buttonRight->isPressed)
	{
		pl->Rotate(0);
		buttonLeft->isPressed = false;
	}
	else if(buttonLeft->isPressed)
	{
		pl->Rotate(1);
		buttonRight->isPressed = false;
	}
	if (buttonThrottle->isPressed)
		pl->Accelerate();

	pl->Move();

	for (std::list<Asteroid *>::iterator o = objects.begin(); o != objects.end(); ++o) 
	{
		(*o)->Move();

		if (pl->CollidesWith(*o))
		{		
			pl->dead = true;
			//LOGI("DEAD!");
		}

		for (std::list<Bullet*>::iterator b = bullets.begin(); b != bullets.end(); ++b) 
		{
			if ((*o)->Contains(*b))
			{
				std::list<Asteroid*>::iterator swap = o;
				o++;			
				objects.erase(swap);
				std::list<Asteroid *> frags = (*swap)->Split();
				objects.merge(frags);
				bullets.erase(b);
				break;
			}			
		}
	}

	if (objects.size() == 0)
		spawn(engine);

	for (std::list<Bullet*>::iterator b = bullets.begin(); b != bullets.end(); ++b) 
	{
		if((*b)->dead)
		{
			std::list<Bullet*>::iterator swap = b;
			b++;
			bullets.erase(swap);
		}
		else
			(*b)->Move();
	}
}

/**
 * Just the current frame in the display.
 */
static void engine_draw_frame(struct engine* engine) {
    if (engine->display == NULL) {
        // No display.
        return;
    }	
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glColorPointer(4, GL_FLOAT, 0, color);
	
	for (std::list<Asteroid*>::iterator r = objects.begin(); r != objects.end(); ++r) 
	{
		(*r)->Draw();
	}

	for (std::list<Bullet*>::iterator r = bullets.begin(); r != bullets.end(); ++r) 
	{
		(*r)->Draw();
	}

	buttonRight->Draw();
	buttonLeft->Draw();
	buttonThrottle->Draw();
	buttonShoot->Draw();

	//if (!pl->dead)
		pl->Draw();

    eglSwapBuffers(engine->display, engine->surface);
	
}

/**
 * Tear down the EGL context currently associated with the display.
 */
static void engine_term_display(struct engine* engine) {
    if (engine->display != EGL_NO_DISPLAY) {
        eglMakeCurrent(engine->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (engine->context != EGL_NO_CONTEXT) {
            eglDestroyContext(engine->display, engine->context);
        }
        if (engine->surface != EGL_NO_SURFACE) {
            eglDestroySurface(engine->display, engine->surface);
        }
        eglTerminate(engine->display);
    }
    engine->animating = 0;
    engine->display = EGL_NO_DISPLAY;
    engine->context = EGL_NO_CONTEXT;
    engine->surface = EGL_NO_SURFACE;
}

/**
 * Process the next input event.
 */
static int32_t engine_handle_input(struct android_app* app, AInputEvent* event) {
	
    struct engine* engine = (struct engine*)app->userData;

    if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) 
	{
        engine->animating = 1;
		
		int action = AKeyEvent_getAction(event);
		float x = engine->state.x = AMotionEvent_getX(event, 0);
		float y = engine->state.y = AMotionEvent_getY(event, 0);
		
		LOGI("Action: %d, Pointers: %d", action, AMotionEvent_getPointerCount(event));
		
        if (action == 0)
		{
			if (buttonRight->Contains(x, y))
			{
				buttonRight->isPressed = true;				
				buttonRight->touchId = 0;
			}
			else if(buttonLeft->Contains(x, y))
			{
				buttonLeft->isPressed = true;
				buttonLeft->touchId = 0;
			}
			else if (buttonThrottle->Contains(x, y))
			{
				buttonThrottle->isPressed = true;
				buttonThrottle->touchId = 0;
			}
			else if (buttonShoot->Contains(x, y))
				bullets.push_back(pl->Shoot());			
		}	
		
		else if(action == 1)
		{
			if (buttonRight->Contains(x, y))
				buttonRight->isPressed = false;
			else if(buttonLeft->Contains(x, y))
				buttonLeft->isPressed = false;
			else if (buttonThrottle->Contains(x, y))
				buttonThrottle->isPressed = false;	
		}

		else if (action == 2)
		{
			if (!buttonRight->Contains(x, y))
				buttonRight->isPressed = false;
			if(!buttonLeft->Contains(x, y))
				buttonLeft->isPressed = false;
			if (!buttonThrottle->Contains(x, y))
				buttonThrottle->isPressed = false;	
		}
		
		if (AMotionEvent_getPointerCount(event) > 1)
		{			
			int action = AKeyEvent_getAction(event);
			float x = engine->state.x = AMotionEvent_getX(event, 1);
			float y = engine->state.y = AMotionEvent_getY(event, 1);
			
			if (action == 261)
			{
				if (buttonRight->Contains(x, y))
				{
					buttonRight->isPressed = true;
					buttonRight->touchId = 1;
				}
				else if(buttonLeft->Contains(x, y))
				{
					buttonLeft->isPressed = true;
					buttonLeft->touchId = 1;
				}
				else if (buttonThrottle->Contains(x, y))
				{
					buttonThrottle->isPressed = true;
					buttonThrottle->touchId = 1;
				}
				else if (buttonShoot->Contains(x, y))
					bullets.push_back(pl->Shoot());					
			}	
		
			else if(action == 262)
			{
				if (buttonRight->Contains(x, y))
				{
					buttonRight->isPressed = false;
					buttonRight->touchId == 999;
				}
				else if(buttonLeft->Contains(x, y))
				{
					buttonLeft->isPressed = false;
					buttonLeft->touchId == 999;
				}
				else if (buttonThrottle->Contains(x, y))
				{
					buttonThrottle->isPressed = false;		
					buttonThrottle->touchId == 999;
				}
			}

			else if(action == 2)
			{
				if (!buttonRight->Contains(x, y) && buttonRight->touchId == 0)
				{
					buttonRight->isPressed = false;
					buttonRight->touchId == 999;
				}
				if(!buttonLeft->Contains(x, y)  && buttonLeft->touchId == 0)
				{
					buttonLeft->isPressed = false;
					buttonLeft->touchId == 999;
				}
				if (!buttonThrottle->Contains(x, y) && buttonThrottle->touchId == 0)
				{
					buttonThrottle->isPressed = false;			
					buttonThrottle->touchId == 999;
				}
			}
		}
    }
    return true;
}

/**
 * Process the next main command.
 */
static void engine_handle_cmd(struct android_app* app, int32_t cmd) {
    struct engine* engine = (struct engine*)app->userData;
    switch (cmd) {
        case APP_CMD_SAVE_STATE:
            // The system has asked us to save our current state.  Do so.
            engine->app->savedState = malloc(sizeof(struct saved_state));
            *((struct saved_state*)engine->app->savedState) = engine->state;
            engine->app->savedStateSize = sizeof(struct saved_state);
            break;
        case APP_CMD_INIT_WINDOW:
            // The window is being shown, get it ready.
            if (engine->app->window != NULL) {
                engine_init_display(engine);
				init_game_objects(engine);
                engine_draw_frame(engine);
            }
            break;
        case APP_CMD_TERM_WINDOW:
            // The window is being hidden or closed, clean it up.
            engine_term_display(engine);
            break;
        case APP_CMD_GAINED_FOCUS:
            // When our app gains focus, we start monitoring the accelerometer.
            if (engine->accelerometerSensor != NULL) {
                ASensorEventQueue_enableSensor(engine->sensorEventQueue,
                        engine->accelerometerSensor);
                // We'd like to get 60 events per second (in us).
                ASensorEventQueue_setEventRate(engine->sensorEventQueue,
                        engine->accelerometerSensor, (1000L/60)*1000);
            }
            break;
        case APP_CMD_LOST_FOCUS:
            // When our app loses focus, we stop monitoring the accelerometer.
            // This is to avoid consuming battery while not being used.
            if (engine->accelerometerSensor != NULL) {
                ASensorEventQueue_disableSensor(engine->sensorEventQueue,
                        engine->accelerometerSensor);
            }
            // Also stop animating.
            engine->animating = 0;
            engine_draw_frame(engine);
            break;
		case APP_CMD_CONFIG_CHANGED:
			if (engine->app->window != NULL && ((engine->width != ANativeWindow_getWidth(app->window)) || (engine->height != ANativeWindow_getHeight(app->window)))) {
				engine_handle_cmd(app, APP_CMD_TERM_WINDOW);
				engine_handle_cmd(app, APP_CMD_INIT_WINDOW);
			}
        break;
    }
}

/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app* state) {
    struct engine engine;
	
    // Make sure glue isn't stripped.
    app_dummy();

    memset(&engine, 0, sizeof(engine));
    state->userData = &engine;
    state->onAppCmd = engine_handle_cmd;
    state->onInputEvent = engine_handle_input;
    engine.app = state;	
    
	// Prepare to monitor accelerometer

    engine.sensorManager = ASensorManager_getInstance();
    engine.accelerometerSensor = ASensorManager_getDefaultSensor(engine.sensorManager, ASENSOR_TYPE_ACCELEROMETER);
    engine.sensorEventQueue = ASensorManager_createEventQueue(engine.sensorManager, state->looper, LOOPER_ID_USER, NULL, NULL);

    if (state->savedState != NULL) {
        // We are starting with a previous saved state; restore from it.
        engine.state = *(struct saved_state*)state->savedState;
    }

    // loop waiting for stuff to do.

    while (1) {
        // Read all pending events.
        int ident;
        int events;
        struct android_poll_source* source;

        // If not animating, we will block forever waiting for events.
        // If animating, we loop until all events are read, then continue
        // to draw the next frame of animation.
        while ((ident=ALooper_pollAll(engine.animating ? 0 : -1, NULL, &events, (void**)&source)) >= 0) 
		{
            // Process this event.
            if (source != NULL) {
                source->process(state, source);
            }

            // If a sensor has data, process it now.
            if (ident == LOOPER_ID_USER) 
			{
                if (engine.accelerometerSensor != NULL) 
				{
                    ASensorEvent event;
					ASensorEventQueue_getEvents(engine.sensorEventQueue, &event, 1);
                }
            }

            // Check if we are exiting.
            if (state->destroyRequested != 0) {
                engine_term_display(&engine);
                return;
            }
        }

		//if(pl->dead)
		//	exit(0); // Yeah that's an overkill

		game_logic_update(&engine);

        if (engine.animating) {
            engine_draw_frame(&engine);
        }
    }
}

