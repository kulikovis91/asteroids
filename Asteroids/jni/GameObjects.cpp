#include "GameObjects.hpp"
#include "common.hpp"

// Player
//------------------------------------------------------------

Player::Player(float _x, float _y)
{
	x = _x;
	y = _y;
	facing = speedx = speedy = 0;
	r = 50;

	poly[0] = x + r * cos((0 + facing) * M_PI / 180);	poly[1] = y + r * sin((0 + facing)* M_PI / 180);
	poly[2] = x + r * cos((140 + facing) * M_PI / 180);	poly[3] = y + r * sin((140 + facing) * M_PI / 180);
	poly[4] = x + r * cos((220 + facing) * M_PI / 180);	poly[5] = y + r * sin((220 + facing)* M_PI / 180);

	RecalculateCollidablePoints();
}

void Player::Move() 
{		
	for(int i = 0; i <= 5; i++)
	{
		if (i % 2 == 0)
			poly[i] += speedx;
		else
			poly[i] += speedy;
	}	

	x += speedx;
	y += speedy;

	if (speedx < 0)
		speedx += SPACE_FRICTION;
	else 
		speedx -= SPACE_FRICTION;
	if (speedy < 0)
		speedy += SPACE_FRICTION;
	else 
		speedy -= SPACE_FRICTION;

	// 720 1280
	
	RecalculateCollidablePoints();

	if (x < 0)
	{
		x = 1300;
		RecalculateGeometry();
	}
	else if (x > 1300)
	{
		x = 0;
		RecalculateGeometry();
	}
	if (y < 0)
	{
		y = 750;
		RecalculateGeometry();
	}
	else if (y > 750)
	{
		y = 0;
		RecalculateGeometry();
	}

	//LOGI("facing: %f, speedx: %f, speedy: %f", facing, speedx, speedy);
}

void Player::Rotate(short dir)
{
	switch(dir)
	{
		case 0:
				facing += 10; // Right
			break;
		case 1:
				facing -= 10; // Left
			break;
	}
	
	if (facing > 360)
		facing = 0;
	if (facing < 0)
		facing = 360;
	
	RecalculateGeometry();
}

void Player::Draw()
{			
	glVertexPointer(2, GL_FLOAT, 0, poly);
	glDrawArrays(GL_LINE_LOOP, 0, 3);	
	glFlush();

	//glVertexPointer(2, GL_FLOAT, 0, collidablePoints);
	//glDrawArrays(GL_LINE_LOOP, 0, 6);	
	//glFlush();
}

void Player::Accelerate()
{
	speedx += (float) PLAYER_ACCELERATION * cos(facing * M_PI / 180);
	speedy += (float) PLAYER_ACCELERATION * sin(facing * M_PI / 180);
}

void Player::RecalculateGeometry()
{
	poly[0] = x + r * cos((0 + facing) * M_PI / 180);		poly[1] = y + r * sin((0 + facing)* M_PI / 180);
	poly[2] = x + r * cos((140 + facing) * M_PI / 180);		poly[3] = y + r * sin((140 + facing) * M_PI / 180);
	poly[4] = x + r * cos((220 + facing) * M_PI / 180);		poly[5] = y + r * sin((220 + facing)* M_PI / 180);

	RecalculateCollidablePoints();
}

void Player::RecalculateCollidablePoints()
{
	collidablePoints[0]	= poly[0];										collidablePoints[1] = poly[1];
	collidablePoints[2]	= (poly[0] + poly[2]) / 2;						collidablePoints[3] = (poly[1] + poly[3]) / 2;
	collidablePoints[4]	= poly[2];										collidablePoints[5] = poly[3];
	collidablePoints[6]	= (poly[2] + poly[4]) / 2;						collidablePoints[7] = (poly[3] + poly[5]) / 2;
	collidablePoints[8]	= poly[4];										collidablePoints[9] = poly[5];
	collidablePoints[10] = (poly[0] + poly[4]) / 2;						collidablePoints[11] = (poly[1] + poly[5]) / 2;
}

Bullet* Player::Shoot()
{
	return new Bullet(poly[0], poly[1], facing);
}

bool Player::CollidesWith(Asteroid *ast)
{
	for (int i = 1; i < 12; i+= 2)
	{
		if (ast->Contains(collidablePoints[i - 1], collidablePoints[i]))
			return true;
	}
	
	return false;
}

// Asteroid 
//------------------------------------------------------------

Asteroid::Asteroid(float _x, float _y, int _vertexes, float _size)
{
	x = _x;
	y = _y;
	size = _size;
	vertexes = _vertexes;
	direction = rand() % 360;

	poly = new GLfloat[vertexes * 2];

	angles = new float[vertexes];

	for (int i = 0; i < vertexes; i++) 
		angles[i] = rand() % 360;

	std::sort(angles, angles + vertexes);

	int j = 0;
	for (int i = 0; i < vertexes; i++)
	{
		poly[j] = x + size * cos(angles[i]  * M_PI / 180);
		poly[j + 1] = y + size * sin(angles[i]  * M_PI / 180);
		j+=2;
	}	
}

Asteroid::~Asteroid()
{
	delete[] poly;
	delete[] angles;
}

void Asteroid::Draw()
{
	glVertexPointer(2, GL_FLOAT, 0, poly);
	glDrawArrays(GL_LINE_LOOP, 0, vertexes);	
	glFlush();
}

bool Asteroid::Contains(float _x, float _y)
{
	if ((_x - x) * (_x - x) + (_y - y) * (_y - y) <= size * size)
		return true;
	else 
		return false;
}

bool Asteroid::Contains(Bullet *b)
{
	if ((b->x - x) * (b->x - x) + (b->y - y) * (b->y - y) <= size * size)
		return true;
	else 
		return false;
}

void Asteroid::Move()
{
	for(int i = 0; i <= vertexes * 2; i++)
	{
		if (i % 2 == 0)
			poly[i] += ROCK_SPEED * cos(direction  * M_PI / 180);

		else
			poly[i] += ROCK_SPEED * sin(direction  * M_PI / 180);
	}	
	
	x += ROCK_SPEED * cos(direction  * M_PI / 180);
	y += ROCK_SPEED * sin(direction  * M_PI / 180);

	if (x < 0)
	{
		x = 1300;
		RecalculateGeometry();
	}
	else if (x > 1300)
	{
		x = 0;
		RecalculateGeometry();
	}
	if (y < 0)
	{
		y = 750;
		RecalculateGeometry();
	}
	else if (y > 750)
	{
		y = 0;
		RecalculateGeometry();
	}	
}

void Asteroid::RecalculateGeometry()
{
	int j = 0;
	for (int i = 0; i < vertexes; i++)
	{
		poly[j] = x + size * cos(angles[i]  * M_PI / 180);
		poly[j + 1] = y + size * sin(angles[i]  * M_PI / 180);
		j+=2;
	}
}

std::list<Asteroid *> Asteroid::Split()
{
	std::list<Asteroid *> objects;
	
	if (size != SMALL_ROCK)
		for (int i = 0; i < 3; i++)
			objects.push_back(new Asteroid(x + (-10 * rand() % 20), y + (-10 * rand() % 20), 10, SMALL_ROCK));

	return objects;
}
// Bullet
//-----------------------------------------------------------

Bullet::Bullet(float _x, float _y, float _direction)
{
	x = _x;
	y = _y;
	direction = _direction;
	trace = 0;
	dead = false;

	poly[0] = _x;
	poly[1] = _y;
}

Bullet::~Bullet()
{

}

void Bullet::Draw() 
{
	glVertexPointer(2, GL_FLOAT, 0, poly);
	glDrawArrays(GL_POINTS, 0, 2);	
	glFlush();
}

void Bullet::Move()
{
	trace += BULLET_SPEED;
	poly[0] = x + BULLET_SPEED * cos(direction * M_PI / 180);
	poly[1] = y + BULLET_SPEED * sin(direction * M_PI / 180);
	x = poly[0];
	y = poly[1];

	if (x < 0)
	{
		x = 1280;
	}
	else if (x > 1280)
	{
		x = 0;
	}
	if (y < 0)
	{
		y = 720;
	}
	else if (y > 720)
	{
		y = 0;
	}

	if (trace > BULLET_RANGE)
		dead = true;
}

// Button 
//-----------------------------------------------------------

Button::Button(float _x, float _y, float _rad)
{		
	poly = new GLfloat[32 * 2];
	rad = _rad;
	isPressed = false;
	x = _x - rad;
	y = _y - rad;
	touchId = 999;

	int j = 0;
	for (int i = 1; i < 32 * 2; i+=2)
	{
		poly[i - 1] = x + rad * cos((j * 360 / 32) * M_PI / 180);
		poly[i] = y + rad * sin((j * 360 / 32) * M_PI / 180);
		j++;
	}
}
		
Button::~Button() 
{
	delete[] poly;
}
		
void Button::Draw() 
{		
	glVertexPointer(2, GL_FLOAT, 0, poly);
	glDrawArrays(GL_LINE_LOOP, 0, 32);	
	glFlush();
}

bool Button::Contains(float _x, float _y)
{
	if ((_x - x) * (_x - x) + (_y - y) * (_y - y) <= rad * rad)
		return true;
	else 
		return false;
}