#include "common.hpp"

class Bullet {
	private:
		GLfloat poly[2];
		float direction, trace;
	public:
		float x, y;
		bool dead;
		Bullet(float _x, float _y, float _direction);
		~Bullet();
		void Draw();
		void Move();
};

class Asteroid {
	private:
		GLfloat *poly;
		float *angles;
		float x, y, direction, size;
		int vertexes;
		void RecalculateGeometry();
	public:
		Asteroid(float _x, float _y, int _vertexes, float _size);
		~Asteroid();
		void Draw();
		void Move();
		bool Contains(float _x, float _y);
		bool Contains(Bullet *b);
		std::list<Asteroid *> Split();
};

class Player  {
	private:
		GLfloat poly[6];
		GLfloat collidablePoints[12];
		void RecalculateGeometry();
		void RecalculateCollidablePoints();
		float speedx, speedy, facing, r;
	public:
		float x, y;
		bool dead;
		Player(float _x, float _y);
		void Move();
		void Accelerate();
		void Rotate(short dir);
		void Draw();
		bool CollidesWith(Asteroid *ast);
		Bullet* Shoot();
};

class Button {
	private:
		GLfloat *poly;
		float x, y;
	public:
		float rad;
		bool isPressed;
		int touchId;
		Button(float _x, float _y, float _rad);
		~Button();
		void Draw();
		bool Contains(float _x, float _y);
};