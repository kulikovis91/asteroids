//BEGIN_INCLUDE(all)

#include <jni.h>
#include <errno.h>
#include <stdlib.h>
#include <cmath>
#include <list>
#include <vector>
#include <algorithm>

#include <EGL/egl.h>
#include <GLES/gl.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "asteroids", __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "asteroids", __VA_ARGS__))

#define BULLET_SPEED				(10.0f)
#define BULLET_RANGE				(720.0f)
#define PLAYER_ACCELERATION			(0.10f)
#define SPACE_FRICTION				(0.01f)
#define BIG_ROCK					(50.0f)
#define SMALL_ROCK					(25.0f)
#define ROCK_SPEED					(2.0f)

//END_INCLUDE(all)